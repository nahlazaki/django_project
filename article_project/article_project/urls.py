from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'article_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^reg/$','article.views.reg'),
    url(r'^save/$','article.views.save'),
    url(r'^articles/all/$','article.views.articles'),
    url(r'^article/get/(?P<article_id>\d+)/$','article.views.article'),
]
