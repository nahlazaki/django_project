from django.contrib import admin
from article.models import User_1,Article,TagOnArticle

# Register your models here.

class TagOnArticleAdmin(admin.StackedInline):
	extra=3
	model=TagOnArticle

class ArticleAdmin(admin.ModelAdmin):
	inlines=[TagOnArticleAdmin]

admin.site.register(Article,ArticleAdmin)

admin.site.register(User_1)
